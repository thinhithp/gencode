CREATE OR REPLACE FUNCTION create_tag(IN p_san_pham_id int8, IN p_tag_id int8, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    INSERT INTO tag (san_pham_id, tag_id) VALUES (p_san_pham_id, p_tag_id);
OPEN v_cursor for SELECT * FROM tag WHERE ID = p_id;
END;
$function$;
----------------------- UPDATE -------------------------------------
CREATE OR REPLACE FUNCTION update_tag(IN p_san_pham_id int8, IN p_tag_id int8, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    UPDATE tag SET tag_id = p_tag_id WHERE san_pham_id = p_san_pham_id;
OPEN v_cursor for SELECT * FROM tag WHERE san_pham_id = p_san_pham_id;
END;
$function$;
---------------------------DETAIL----------------------
CREATE OR REPLACE FUNCTION view_tag(IN p_id INTEGER, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    OPEN v_cursor FOR SELECT san_pham_id, tag_id FROM tag WHERE ID = p_id;
END;
$function$;
--------------------DELETE---------------------
CREATE OR REPLACE FUNCTION delete_tag(IN p_id INTEGER, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM tag WHERE ID = p_id;
END;
$function$;
