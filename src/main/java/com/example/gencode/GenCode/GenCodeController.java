package com.example.gencode.GenCode;


import com.example.gencode.GenCode.Utils.ApiConstants;
import com.example.gencode.GenCode.dto.GenCodeReadTableDatabaseDto;
import com.example.gencode.GenCode.service.GenCodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstants.BASE + ApiConstants.GenCode.BASE)
@CrossOrigin("*")
public class GenCodeController {
	@Autowired
	private GenCodeService genCodeService;

	@PostMapping("/gen-code")
	public File readTable(@RequestBody GenCodeReadTableDatabaseDto genCodeReadTableDatabaseDto) throws IOException {
		return this.genCodeService.readTableAndWriteToXml(genCodeReadTableDatabaseDto);
	}
	@PostMapping("/download")
	public ResponseEntity<ByteArrayResource> downloadFile(@RequestBody List<String> fileName) {
		return this.genCodeService.downloadFiles(fileName);
	}
	@GetMapping("/read-file")
	public List<String> readFile() throws IOException{
		return this.genCodeService.listFileNamesInFolder();
	}
}
