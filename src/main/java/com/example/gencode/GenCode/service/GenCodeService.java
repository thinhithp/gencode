package com.example.gencode.GenCode.service;

import com.example.gencode.GenCode.dto.GenCodeColumnDetailDto;
import com.example.gencode.GenCode.dto.GenCodeReadTableDatabaseDto;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@ComponentScan
@Service
public class GenCodeService {
	//	@Autowired
	//	private DataSource dataSource;
	@Value("${output.folder}")
	private String outputFolder;

	@Autowired
	private GenCodeDatabaseConnectService connectService;
	@Autowired
	private GenCodeEntityService genCodeEntityService;

	@Autowired
	private GenCodeProcedureService generateCrudProcedures;

	@Getter
	private Set<String> validTableNames = new HashSet<>();

	public File readTableAndWriteToXml(GenCodeReadTableDatabaseDto genCodeReadTableDatabaseDto) throws IOException {
		DataSource dataSource = this.connectService.createDataSource(genCodeReadTableDatabaseDto.getUrl(),
				genCodeReadTableDatabaseDto.getUseName(), genCodeReadTableDatabaseDto.getPassword());
		File outputDir = new File(outputFolder);
		if (!outputDir.exists()) {
			if (!outputDir.mkdirs()) {
				throw new IOException("Failed to create directory: " + outputDir.getAbsolutePath());
			}
		}

		// Iterate over each table name in the list
		for (String tableName : genCodeReadTableDatabaseDto.getTable()) {
			File xmlFile = new File(outputDir, tableName.concat(".xml"));

			try (Connection conn = dataSource.getConnection()) {
				Map<String, String> foreignKeys = this.connectService.getForeignKeys(conn, tableName);
				Map<String, GenCodeColumnDetailDto> columnDetails = this.connectService.getColumnDetails(
						genCodeReadTableDatabaseDto);

				DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				Element root = document.createElement("Table");
				document.appendChild(root);

				try (Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName)) {
					ResultSetMetaData metaData = rs.getMetaData();
					int columnCount = metaData.getColumnCount();

					for (int i = 1; i <= columnCount; i++) {
						Element columnElement = document.createElement("Column");
						columnElement.setAttribute("name", metaData.getColumnName(i));
						columnElement.setAttribute("type", metaData.getColumnTypeName(i));
						columnElement.setAttribute("size", String.valueOf(metaData.getColumnDisplaySize(i)));
						columnElement.setAttribute("nullable",
								String.valueOf(metaData.isNullable(i) == ResultSetMetaData.columnNullable));

						GenCodeColumnDetailDto detail = columnDetails.get(metaData.getColumnName(i));
						if (detail != null) {
							columnElement.setAttribute("defaultValue", detail.getDefaultValue());
							columnElement.setAttribute("description", detail.getDescription());
						}

						String foreignKey = foreignKeys.getOrDefault(metaData.getColumnName(i), null);
						if (foreignKey != null) {
							columnElement.setAttribute("foreignKey", foreignKey);
						}

						root.appendChild(columnElement);
					}
				}
				try (FileWriter writer = new FileWriter(xmlFile)) {
					writer.write(convertDocumentToString(document));
				}

				this.genCodeEntityService.generateEntityFromXml(xmlFile, tableName);
				this.generateCrudProcedures.generateCrudProcedures(genCodeReadTableDatabaseDto, xmlFile);
				this.deleteXmlFile(xmlFile);
			} catch (Exception e) {
				e.printStackTrace();
				// Consider how to handle exceptions for individual tables
			}
		}

		return outputDir;
	}

	public List<String> listFileNamesInFolder() throws IOException {
		Path folderPath = Paths.get(outputFolder);

		if (!Files.isDirectory(folderPath)) {
			throw new IOException("Specified path is not a directory");
		}

		List<String> fileNames = new ArrayList<>();
		try (Stream<Path> paths = Files.walk(folderPath)) {
			fileNames = paths.filter(Files::isRegularFile)
					.map(filePath -> filePath.getFileName().toString())
					.collect(Collectors.toList());
		}

		return fileNames;
	}

	public ResponseEntity<ByteArrayResource> downloadFiles(List<String> fileNames) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ZipOutputStream zos = new ZipOutputStream(baos)) {

			for (String fileName : fileNames) {
				Path filePath = Paths.get(outputFolder, fileName);
				if (Files.exists(filePath)) {
					ZipEntry zipEntry = new ZipEntry(fileName);
					zos.putNextEntry(zipEntry);
					byte[] data = Files.readAllBytes(filePath);
					zos.write(data);
					zos.closeEntry();
				}
			}

			zos.finish();
			ByteArrayResource resource = new ByteArrayResource(baos.toByteArray());

			String zipFileName = "download.zip";
			ResponseEntity<ByteArrayResource> response = ResponseEntity.ok()
					.header("Content-Disposition", "attachment; filename=" + zipFileName)
					.contentLength(baos.size())
					.body(resource);

			// Schedule file deletion
			scheduleFileDeletion(fileNames, 5); // 30 seconds delay

			return response;
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().build();
		}
	}

	private void scheduleFileDeletion(List<String> fileNames, int delayInSeconds) {
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.schedule(() -> {
			for (String fileName : fileNames) {
				try {
					Path filePath = Paths.get(outputFolder, fileName);
					if (Files.exists(filePath)) {
						Files.delete(filePath);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}, delayInSeconds, TimeUnit.SECONDS);
		executorService.shutdown();
	}
public boolean deleteXmlFile(File xmlFile) {
	if (xmlFile != null && xmlFile.exists()) {
		return xmlFile.delete();
	}
	return false;
}

// Method to convert Document to String
private String convertDocumentToString(Document doc) throws TransformerException {
	TransformerFactory tf = TransformerFactory.newInstance();
	Transformer transformer = tf.newTransformer();
	StringWriter writer = new StringWriter();
	transformer.transform(new DOMSource(doc), new StreamResult(writer));
	return writer.getBuffer().toString();
}
}
