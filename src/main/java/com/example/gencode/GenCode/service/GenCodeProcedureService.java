package com.example.gencode.GenCode.service;

import com.example.gencode.GenCode.Utils.ConvertString;
import com.example.gencode.GenCode.dto.GenCodeReadTableDatabaseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

@Service
@ComponentScan
public class GenCodeProcedureService {
	@Value("${output.folder}")
	private String outputFolder;
	@Autowired
	private GenCodeDatabaseConnectService connectService;

	public String generateCrudProcedures(GenCodeReadTableDatabaseDto baseQuerry, File xmlFile) throws SQLException {
		DataSource dataSource = this.connectService.createDataSource(baseQuerry.getUrl(), baseQuerry.getUseName(),
				baseQuerry.getPassword());
		String databaseProductName;
		try (Connection conn = dataSource.getConnection()) {
			DatabaseMetaData metaData = conn.getMetaData();
			databaseProductName = metaData.getDatabaseProductName();
		}

		StringBuilder allProcedures = new StringBuilder();

		for (String tableName : baseQuerry.getTable()) {
			String tableNameConvert = ConvertString.convertToCamelCase(tableName).concat("Procedure");
			File procedureFile = new File(outputFolder, tableNameConvert.concat(".txt"));

			StringBuilder procedures = new StringBuilder();
			try {
				DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
				Document document = documentBuilder.parse(xmlFile);
				document.getDocumentElement().normalize();

				NodeList columnList = document.getElementsByTagName("Column");

				// Generate CRUD procedures for each table
				procedures.append(generateCreateProcedure(columnList, tableName, databaseProductName));
				procedures.append(generateUpdateProcedure(columnList, tableName, databaseProductName));
				procedures.append(generateReadProcedure(columnList, tableName, databaseProductName));
				procedures.append(generateDeleteProcedure(columnList, tableName, databaseProductName));

				// Write the procedures to a file
				try (FileWriter writer = new FileWriter(procedureFile)) {
					writer.write(procedures.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Error generating CRUD procedures for table " + tableName + ": " + e.getMessage();
			}

			allProcedures.append(procedureFile.getAbsolutePath()).append("\n");
		}

		return allProcedures.toString();
	}


	private String generateCreateProcedure(NodeList columnList, String tableName, String databaseType) {
		StringBuilder procedure = new StringBuilder();
		StringBuilder parameters = new StringBuilder();
		StringBuilder insertValues = new StringBuilder();
		StringBuilder valuesPlaceholders = new StringBuilder();
		String dbTypeLower = databaseType.toLowerCase().trim();

		// Construct parameters, insert values, and placeholders based on columns
		for (int i = 0; i < columnList.getLength(); i++) {
			Element column = (Element) columnList.item(i);
			String columnName = column.getAttribute("name");
			String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

			// Construct parameters and placeholders based on the database type
			if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
				parameters.append("@p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("@p_").append(columnName.toLowerCase());
			} else if (dbTypeLower.contains("oracle")) {
				parameters.append("p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			} else {
				// For other databases like MySQL and PostgreSQL
				parameters.append("IN ".concat("p_".concat(columnName.toLowerCase()))).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			}

			insertValues.append(columnName);
			if (i < columnList.getLength() - 1) {
				parameters.append(", ");
				insertValues.append(", ");
				valuesPlaceholders.append(", ");
			}
		}

		if (dbTypeLower.contains("mysql")) {
			procedure.append("DELIMITER $$\n").append("CREATE PROCEDURE create_").append(tableName).append("(")
					.append(parameters).append(")\n").append("BEGIN\n").append("    INSERT INTO ").append(tableName)
					.append(" (").append(insertValues).append(") VALUES (").append(valuesPlaceholders).append(");\n")
					.append("END$$\nDELIMITER ;\n");
		} else if (dbTypeLower.contains("postgresql")) {
			procedure.append("CREATE OR REPLACE FUNCTION create_").append(tableName).append("(").append(parameters)
					.append(", OUT v_cursor refcursor").append(")\n").append("LANGUAGE plpgsql\n")
					.append("AS $function$\n").append("BEGIN\n").append("    INSERT INTO ").append(tableName)
					.append(" (").append(insertValues).append(") VALUES (").append(valuesPlaceholders).append(");\n")
					.append("OPEN v_cursor for SELECT * FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END;\n").append("$function$;\n");

		} else if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
			// SQL Server specific code
			procedure.append("CREATE PROCEDURE PKG_CREATE_").append(tableName).append(" ").append(parameters)
					.append(" \n").append("AS\n").append("BEGIN\n").append("    SET NOCOUNT ON;\n")
					.append("    INSERT INTO ").append(tableName).append(" (").append(insertValues).append(") VALUES (")
					.append(valuesPlaceholders).append(");\n").append("SELECT * FROM ").append(tableName)
					.append(" WHERE ID = @p_id;\n").append("END;\n");
		} else if (dbTypeLower.contains("oracle")) {
			procedure.append("CREATE OR REPLACE PROCEDURE create_").append(tableName).append("(");

			// Oracle parameters use a different naming convention (usually without @)
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");
				String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

				procedure.append("p_").append(columnName).append(" ").append(columnType);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(") \n").append("IS\n").append("BEGIN\n").append("    INSERT INTO ").append(tableName)
					.append(" (").append(insertValues).append(") VALUES (");

			// Insert the parameter placeholders
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append("p_").append(columnName);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(");\n").append("END;\n");
		} else {
			return "Unsupported database type: " + databaseType;
		}

		return procedure.toString();
	}

	private String generateUpdateProcedure(NodeList columnList, String tableName, String databaseType) {
		StringBuilder procedure = new StringBuilder();
		StringBuilder parameters = new StringBuilder();
		StringBuilder updateValues = new StringBuilder();
		StringBuilder valuesPlaceholders = new StringBuilder();
		String dbTypeLower = databaseType.toLowerCase().trim();

		// Construct parameters, insert values, and placeholders based on columns
		for (int i = 0; i < columnList.getLength(); i++) {
			Element column = (Element) columnList.item(i);
			String columnName = column.getAttribute("name");
			String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

			// Construct parameters and placeholders based on the database type
			if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
				parameters.append("@p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("@p_").append(columnName.toLowerCase());
			} else if (dbTypeLower.contains("oracle")) {
				parameters.append("p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			} else {
				// For other databases like MySQL and PostgreSQL
				parameters.append("IN ".concat("p_".concat(columnName.toLowerCase()))).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			}

			updateValues.append(columnName);
			if (i < columnList.getLength() - 1) {
				parameters.append(", ");
				updateValues.append(", ");
				valuesPlaceholders.append(", ");
			}
		}

		if (dbTypeLower.contains("mysql")) {
			String[] placeholderArray = valuesPlaceholders.toString().split(", ");

			procedure.append("DELIMITER $$\n").append("CREATE PROCEDURE update_").append(tableName).append("(")
					.append(parameters).append(")\n").append("BEGIN\n").append("    UPDATE ").append(tableName)
					.append(" SET ");

			// Assuming the first column is the primary key used in the WHERE clause
			for (int i = 1; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append(columnName).append(" = ").append(placeholderArray[i - 1]);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			// Use the first column as the key for the WHERE clause
			Element keyColumn = (Element) columnList.item(0);
			String keyColumnName = keyColumn.getAttribute("name");

			procedure.append(" WHERE ").append(keyColumnName).append(" = ").append(placeholderArray[0]).append(";\n")
					.append("END$$\nDELIMITER ;\n");

		} else if (dbTypeLower.contains("postgresql")) {
			procedure.append("----------------------- UPDATE -------------------------------------\n")
					.append("CREATE OR REPLACE FUNCTION update_").append(tableName).append("(").append(parameters)
					.append(", OUT v_cursor refcursor").append(")\n").append("LANGUAGE plpgsql\n")
					.append("AS $function$\n").append("BEGIN\n").append("    UPDATE ").append(tableName)
					.append(" SET ");

			// Assuming the first column is the primary key used in the WHERE clause
			for (int i = 1; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");
				String placeholder = "p_" + columnName.toLowerCase();

				procedure.append(columnName).append(" = ").append(placeholder);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			// Use the first column as the key for the WHERE clause
			Element keyColumn = (Element) columnList.item(0);
			String keyColumnName = keyColumn.getAttribute("name");
			String keyPlaceholder = "p_" + keyColumnName.toLowerCase();

			procedure.append(" WHERE ").append(keyColumnName).append(" = ").append(keyPlaceholder).append(";\n")
					.append("OPEN v_cursor for SELECT * FROM ").append(tableName).append(" WHERE ")
					.append(keyColumnName).append(" = ").append(keyPlaceholder).append(";\n").append("END;\n")
					.append("$function$;\n");

		} else if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
			// SQL Server specific code
			procedure.append("----------------------- UPDATE -------------------------------------\n")
					.append("CREATE PROCEDURE PKG_UPDATE_").append(tableName).append(" ").append(parameters)
					.append(" \n").append("AS\n").append("BEGIN\n").append("    SET NOCOUNT ON;\n")
					.append("    UPDATE ").append(tableName).append(" SET ");

			// Assuming the first parameter is the primary key used in the WHERE clause
			for (int i = 1; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");
				String placeholder = "@p_" + columnName.toLowerCase();

				procedure.append(columnName).append(" = ").append(placeholder);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			// Use the first parameter as the key for the WHERE clause
			Element keyColumn = (Element) columnList.item(0);
			String keyColumnName = keyColumn.getAttribute("name");
			String keyPlaceholder = "@p_" + keyColumnName.toLowerCase();

			procedure.append(" WHERE ").append(keyColumnName).append(" = ").append(keyPlaceholder).append(";\n")
					.append("SELECT * FROM ").append(tableName).append(" WHERE ").append(keyColumnName).append(" = ")
					.append(keyPlaceholder).append(";\n").append("END;\n");
		} else if (dbTypeLower.contains("oracle")) {
			procedure.append("CREATE OR REPLACE PROCEDURE update_").append(tableName).append("(");

			// Oracle parameters use a different naming convention (usually without @)
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");
				String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

				procedure.append("p_").append(columnName).append(" ").append(columnType);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(") \n").append("IS\n").append("BEGIN\n").append("    UPDATE ").append(tableName)
					.append(" SET ");

			// Assuming the first parameter is the primary key used in the WHERE clause
			for (int i = 1; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");
				String placeholder = "p_" + columnName;

				procedure.append(columnName).append(" = ").append(placeholder);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			// Use the first parameter as the key for the WHERE clause
			Element keyColumn = (Element) columnList.item(0);
			String keyColumnName = keyColumn.getAttribute("name");
			String keyPlaceholder = "p_" + keyColumnName;

			procedure.append(" WHERE ").append(keyColumnName).append(" = ").append(keyPlaceholder).append(";\n")
					.append("END;\n");

		} else {
			return "Unsupported database type: " + databaseType;
		}

		return procedure.toString();
	}

	private String generateReadProcedure(NodeList columnList, String tableName, String databaseType) {
		StringBuilder procedure = new StringBuilder();
		StringBuilder parameters = new StringBuilder();
		StringBuilder insertValues = new StringBuilder();
		StringBuilder valuesPlaceholders = new StringBuilder();
		String dbTypeLower = databaseType.toLowerCase().trim();

		// Construct parameters, insert values, and placeholders based on columns
		for (int i = 0; i < columnList.getLength(); i++) {
			Element column = (Element) columnList.item(i);
			String columnName = column.getAttribute("name");
			String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

			// Construct parameters and placeholders based on the database type
			if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
				parameters.append("@p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("@p_").append(columnName.toLowerCase());
			} else if (dbTypeLower.contains("oracle")) {
				parameters.append("p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			} else {
				// For other databases like MySQL and PostgreSQL
				parameters.append("IN ".concat("p_".concat(columnName.toLowerCase()))).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			}

			insertValues.append(columnName);
			if (i < columnList.getLength() - 1) {
				parameters.append(", ");
				insertValues.append(", ");
				valuesPlaceholders.append(", ");
			}
		}

		if (dbTypeLower.contains("mysql")) {
			procedure
					.append("DELIMITER $$\n")
					.append("CREATE PROCEDURE view_").append(tableName).append("(IN p_id ")
					.append(mapSqlTypeToDbType("INT", databaseType)) // Replace "id_column_type" with actual data type of ID
					.append(")\n")
					.append("BEGIN\n")
					.append("    SELECT ");

			// Iterate over the columnList to build the SELECT statement
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append(columnName);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(" FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END$$\nDELIMITER ;\n");
		} else if (dbTypeLower.contains("postgresql")) {
			procedure.append("---------------------------DETAIL----------------------\n")
					.append("CREATE OR REPLACE FUNCTION view_").append(tableName).append("(IN p_id ")
					.append(mapSqlTypeToDbType("INT", databaseType)) // Replace "id_column_type" with actual data type of ID
					.append(", OUT v_cursor refcursor").append(")\n")
					.append("LANGUAGE plpgsql\n")
					.append("AS $function$\n")
					.append("BEGIN\n")
					.append("    OPEN v_cursor FOR SELECT ");

			// Iterate over the columnList to build the SELECT statement
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append(columnName);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(" FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END;\n")
					.append("$function$;\n");

		} else if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
			procedure.append("CREATE PROCEDURE PKG_VIEW_").append(tableName).append(" ")
					.append("@p_id ").append(mapSqlTypeToDbType("BIGINT", "sqlserver"))
					.append(" AS\n")
					.append("BEGIN\n")
					.append("    SET NOCOUNT ON;\n")
					.append("    SELECT ");

			// Iterate over the columnList to build the SELECT statement
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append(columnName);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(" FROM ").append(tableName).append(" WHERE ID = @p_id;\n")
					.append("END;\n");

		} else if (dbTypeLower.contains("oracle")) {
			procedure.append("CREATE OR REPLACE PROCEDURE view_").append(tableName).append("(")
					.append("p_id ").append(mapSqlTypeToDbType("INT", "oracle"))
					.append(", OUT v_cursor SYS_REFCURSOR").append(")\n")
					.append("IS\n")
					.append("BEGIN\n")
					.append("    OPEN v_cursor FOR SELECT ");

			// Iterate over the columnList to build the SELECT statement
			for (int i = 0; i < columnList.getLength(); i++) {
				Element column = (Element) columnList.item(i);
				String columnName = column.getAttribute("name");

				procedure.append(columnName);
				if (i < columnList.getLength() - 1) {
					procedure.append(", ");
				}
			}

			procedure.append(" FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END;\n");

		} else {
			return "Unsupported database type: " + databaseType;
		}

		return procedure.toString();
	}

	private String generateDeleteProcedure(NodeList columnList, String tableName, String databaseType) {
		StringBuilder procedure = new StringBuilder();
		StringBuilder parameters = new StringBuilder();
		StringBuilder insertValues = new StringBuilder();
		StringBuilder valuesPlaceholders = new StringBuilder();
		String dbTypeLower = databaseType.toLowerCase().trim();

		// Construct parameters, insert values, and placeholders based on columns
		for (int i = 0; i < columnList.getLength(); i++) {
			Element column = (Element) columnList.item(i);
			String columnName = column.getAttribute("name");
			String columnType = mapSqlTypeToDbType(column.getAttribute("type"), databaseType);

			// Construct parameters and placeholders based on the database type
			if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
				parameters.append("@p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("@p_").append(columnName.toLowerCase());
			} else if (dbTypeLower.contains("oracle")) {
				parameters.append("p_").append(columnName.toLowerCase()).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			} else {
				// For other databases like MySQL and PostgreSQL
				parameters.append("IN ".concat("p_".concat(columnName.toLowerCase()))).append(" ").append(columnType);
				valuesPlaceholders.append("p_").append(columnName.toLowerCase());
			}

			insertValues.append(columnName);
			if (i < columnList.getLength() - 1) {
				parameters.append(", ");
				insertValues.append(", ");
				valuesPlaceholders.append(", ");
			}
		}

		if (dbTypeLower.contains("mysql")) {
			procedure.append("DELIMITER $$\n")
					.append("CREATE PROCEDURE delete_").append(tableName).append("(IN p_id ")
					.append(mapSqlTypeToDbType("INT", "mysql")) // Replace "id_column_type" with actual data type of ID
					.append(")\n")
					.append("BEGIN\n")
					.append("    DELETE FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END$$\nDELIMITER ;\n");
		} else if (dbTypeLower.contains("postgresql")) {
			procedure
					.append("--------------------DELETE---------------------\n")
					.append("CREATE OR REPLACE FUNCTION delete_").append(tableName).append("(IN p_id ")
					.append(mapSqlTypeToDbType("INT", "postgresql")) // Replace "id_column_type" with actual data type of ID
					.append(", OUT v_cursor refcursor").append(")\n")
					.append("LANGUAGE plpgsql\n")
					.append("AS $function$\n")
					.append("BEGIN\n")
					.append("    DELETE FROM ").append(tableName).append(" WHERE ID = p_id;\n");

			// Optionally, open the cursor for a SELECT statement if you need to return some data
			// procedure.append("    OPEN v_cursor FOR SELECT ... ;\n");

			procedure.append("END;\n")
					.append("$function$;\n");


		} else if (dbTypeLower.contains("microsoft sql server") || dbTypeLower.contains("sql server")) {
			// SQL Server specific code
			procedure.append("CREATE PROCEDURE PKG_DELETE_").append(tableName).append(" ")
					.append("@p_id ").append(mapSqlTypeToDbType("INT", "sqlserver")) // Replace "id_column_type" with actual data type of ID
					.append(" AS\n")
					.append("BEGIN\n")
					.append("    SET NOCOUNT ON;\n")
					.append("    DELETE FROM ").append(tableName).append(" WHERE ID = @p_id;\n")
					.append("END;\n");

		} else if (dbTypeLower.contains("oracle")) {
			procedure.append("CREATE OR REPLACE PROCEDURE delete_").append(tableName).append("(p_id ")
					.append(mapSqlTypeToDbType("INT", "oracle")) // Replace "id_column_type" with the actual data type of ID
					.append(")\n")
					.append("IS\n")
					.append("BEGIN\n")
					.append("    DELETE FROM ").append(tableName).append(" WHERE ID = p_id;\n")
					.append("END;\n");

		} else {
			return "Unsupported database type: " + databaseType;
		}

		return procedure.toString();
	}

	private String mapSqlTypeToDbType(String sqlType, String databaseType) {
		// Normalize SQL type and database type to lowercase
		sqlType = sqlType.toLowerCase();
		databaseType = databaseType.toLowerCase();

		switch (databaseType) {
		case "mysql":
			return mapSqlTypeToMySqlType(sqlType);
		case "postgresql":
			return mapSqlTypeToPostgreSqlType(sqlType);
		case "microsoft sql server":
			return mapSqlTypeToSqlServerType(sqlType);
		case "oracle":
			return mapSqlTypeToOracleType(sqlType);
		default:
			throw new IllegalArgumentException("Unsupported database type: " + databaseType);
		}
	}

	private String mapSqlTypeToMySqlType(String sqlType) {
		// Provide mapping for MySQL types
		// Example:
		switch (sqlType) {
		case "varchar":
			return "VARCHAR(255)"; // Adjust size as needed
		case "int":
			return "INT";
		// ... other mappings ...
		default:
			return sqlType;
		}
	}

	private String mapSqlTypeToPostgreSqlType(String sqlType) {
		// Provide mapping for PostgreSQL types
		// Example:
		switch (sqlType) {
		case "varchar":
			return "VARCHAR(255)"; // Adjust size as needed
		case "int":
			return "INTEGER";
		// ... other mappings ...
		default:
			return sqlType;
		}
	}

	private String mapSqlTypeToSqlServerType(String sqlType) {
		// Provide mapping for SQL Server types
		// Example:
		switch (sqlType) {
		case "varchar":
			return "NVARCHAR(255)"; // Adjust size as needed
		case "int":
			return "INT";
		// ... other mappings ...
		default:
			return sqlType;
		}
	}

	private String mapSqlTypeToOracleType(String sqlType) {
		// Provide mapping for Oracle types
		// Example:
		switch (sqlType) {
		case "varchar":
			return "VARCHAR2(255)"; // Adjust size as needed
		case "int":
			return "NUMBER";
		// ... other mappings ...
		default:
			return sqlType;
		}
	}

}
