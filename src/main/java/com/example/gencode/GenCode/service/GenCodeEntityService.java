package com.example.gencode.GenCode.service;

import com.example.gencode.GenCode.Utils.ConvertString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileWriter;

@Service
@ComponentScan
public class GenCodeEntityService {
	@Value("${output.folder}")
	private String outputFolder;
	@Autowired
	private GenCodeDatabaseConnectService connectService;

	public File generateEntityFromXml(File xmlFile, String table) {
		String tableNameConvert = ConvertString.convertToCamelCase(table).concat("Entity");
		File javaFile = new File(outputFolder, tableNameConvert.concat(".txt"));
		File txtFile = null;
		try {
			// Parse the XML file
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(xmlFile);
			document.getDocumentElement().normalize();

			//String tableName = document.getDocumentElement().getNodeName();
			String entityName = ConvertString.convertToCamelCase(table).concat("Entity");

			NodeList columnList = document.getElementsByTagName("Column");

			// Start building the Java class code
			StringBuilder classCode = new StringBuilder();
			classCode.append("@Entity\n");
			classCode.append("@Table(name = \"").append(table.toUpperCase()).append("\")\n");
			classCode.append("@Getter\n");
			classCode.append("@Setter\n");
			classCode.append("@AllArgsConstructor\n");
			classCode.append("@NoArgsConstructor\n");
			classCode.append("@Builder\n");
			classCode.append("public class ").append(entityName).append(" {\n");

			for (int i = 0; i < columnList.getLength(); i++) {
				Element columnElement = (Element) columnList.item(i);
				String columnName = ConvertString.convertToCamelCase(columnElement.getAttribute("name"), false);
				String columnType = mapSqlTypeToJavaType(columnElement.getAttribute("type"));
				String nullable = columnElement.getAttribute("nullable");
				String comment = columnElement.getAttribute("description");
				String columnSize = columnElement.getAttribute("size");

				// Generate @Comment annotation
				if (!comment.isEmpty()) {
					classCode.append("\t@Comment(\"").append(comment).append("\")\n");
				}
				// Generate @Column annotation
				classCode.append("\t@Column(name = \"").append(columnElement.getAttribute("name")).append("\"");
				if (!"true".equals(nullable)) {
					classCode.append(", nullable = false");
				}
				if ("String".equals(columnType) && !columnSize.isEmpty()) {
					classCode.append(", length = ").append(columnSize);
				}
				classCode.append(")\n");

				// Generate @Size or @Max annotation for non-String fields
				if (!"String".equals(columnType) && !columnSize.isEmpty()) {
					// Choose between @Size and @Max based on your specific requirements
					classCode.append("\t@Size(max = ").append(columnSize).append(")\n");
					// or classCode.append("\t@Max(").append(columnSize).append(")\n");
				}

				// Generate field
				classCode.append("\tprivate ").append(columnType).append(" ").append(columnName).append(";\n");
				// You can also generate getters and setters here
			}
			classCode.append("}\n");
			// Write the class code to a .txt file
			//	txtFile = new File(entityName + ".txt");
			try (FileWriter writer = new FileWriter(javaFile)) {
				writer.write(classCode.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return txtFile;
	}
	private String mapSqlTypeToJavaType(String sqlType) {
		sqlType = sqlType.toLowerCase(); // Normalize the SQL type to lowercase

		switch (sqlType) {
		case "bigint":
			return "Long";
		case "int":
		case "integer":
		case "smallint":
		case "tinyint":
			return "Integer";
		case "bit":
			return "Boolean";
		case "varchar":
		case "nvarchar":
		case "char":
		case "text":
			return "String";
		case "float":
			return "Float";
		case "double":
			return "Double";
		case "decimal":
		case "numeric":
			return "BigDecimal";
		case "date":
		case "datetime":
		case "timestamp":
			return "Date"; // or Date, depending on your needs
		case "time":
			return "Date"; // or Time
		case "blob":
		case "binary":
		case "varbinary":
			return "byte[]";
		// ... additional mappings for other SQL types ...
		default:
			return "Object";
		}
	}
}
