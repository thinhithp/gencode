package com.example.gencode.GenCode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenCodeColumnDetailDto {
	private String columnName;
	private String defaultValue;
	private String description;
}
