package com.example.gencode.GenCode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenCodeColumnDescriptionDto {
	private String columnName;
	private String description;
}
