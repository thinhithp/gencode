package com.example.gencode.GenCode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenCodeReadTableDatabaseDto {
	private List<String> table;
	private String url;
	private String useName;
	private String password;
}
