package com.example.gencode.GenCode.Utils;

public class ConvertString {

	private ConvertString() {
		throw new UnsupportedOperationException();
	}
	public static String convertToCamelCase(String input) {
		return convertToCamelCase(input, true);
	}

	public static String convertToCamelCase(String input, boolean startWithCapital) {
		StringBuilder result = new StringBuilder();
		boolean capitalizeNext = startWithCapital;

		for (char c : input.toCharArray()) {
			if (c == '_' || c == ' ') {
				capitalizeNext = true;
			} else {
				if (capitalizeNext) {
					result.append(Character.toUpperCase(c));
					capitalizeNext = false;
				} else {
					result.append(Character.toLowerCase(c));
				}
			}
		}

		return result.toString();
	}
}
