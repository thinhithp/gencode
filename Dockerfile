# Giai đoạn 1: Xây dựng ứng dụng với JDK 17
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src /app/src
RUN mvn -f pom.xml -e clean package
RUN ls /app/target

# Giai đoạn 2: Tạo Docker image cuối cùng với OpenJDK 17
FROM openjdk:17
WORKDIR /app
COPY --from=build /app/target/demo-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]
